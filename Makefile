all: stage0 stage1 stage2 stage3 stage4

.PHONY: all

.NOTPARALLEL: all

.DEFAULT: all

CNTDIR := containerfiles
STG1DIR := $(CNTDIR)/stage1/default
STG2DIR := $(CNTDIR)/stage2/default
STG3DIR := $(CNTDIR)/stage3/default
STG4DIR := $(CNTDIR)/stage4/default

OPTS := --cap-add=CAP_SYS_ADMIN \
				--cap-add=CAP_NET_ADMIN \
				--cap-add=CAP_MKNOD \
				--cap-add=CAP_SYS_PTRACE \
				--pull-always

CURRENT_UID := $(shell id -u)

list:
	@tree $(CNTDIR)

.PHONY: list

targets:
	@echo "Installable targets are:"
	@echo "all"
	@echo "├── stage0"
	@echo "│   ├── portage"
	@echo "│   └── catalyst"
	@echo "├── stage1"
	@echo "│   └── stage1-amd64-default"
	@echo "├── stage2"
	@echo "│   └── stage2-amd64-default"
	@echo "├── stage3"
	@echo "│   └── stage3-amd64-default"
	@echo "└── stage4"
	@echo "    ├── stage4-amd64-default"
	@echo "    └── stage4-extra <---------------------- This is the only parallelizable portion"
	@echo "        ├── installer"
	@echo "        └── stage4-amd64-default-zfs"
	@echo
	@echo "The default target is 'all'"

.PHONY: targets

seed:
	@./getSeed.sh

.PHONY: seed

clean-seed:
	@rm -vf stage3*.tar.xz

.PHONY: clean-seed

sudo-test:
ifneq ($(CURRENT_UID), 0)
	@echo "Please run as sudo as CAP_MKNOD is needed for stage builds" 
	@exit 1
endif

cred-test: get-cred
	@if [ "$(QUSER)" != "" ]; then\
			echo "Quay User is $(QUSER)";\
			echo "Quay Pass is $(QPASS)";\
	fi
	@if [ "$(DUSER)" != "" ]; then\
			echo "Docker User is $(DUSER)";\
			echo "Docker Pass is $(DPASS)";\
	fi

.PHONY: cred-test

get-cred: get-cred-quay

#get-cred: get-cred-quay get-cred-docker get-cred-gpg

.PHONY: get-cred

.NOTPARALLEL: get-cred

get-cred-quay: cred-prompt-quay
ifeq ($(QUSER),)
	$(eval QUSER=$(shell read -p "Username: " u ; echo $$u))
endif
ifeq ($(QPASS),)
	$(eval QPASS=$(shell read -srp "Password: " p ; echo $$p | sed 's/#/\\#/g'))
endif
	@echo

.PHONY: get-cred-quay

cred-prompt-quay:
	@echo "Please enter quay.io credentials, leave blank to skip"

.PHONY: cred-prompt-quay

get-cred-gpg:
ifneq ($(GUSER),)
		@echo "Signing with $(GUSER)"
		$(eval SIGN_OPTS=$(shell echo "--sign-by $(GUSER) --sign-passphrase-file $(GFILE)"))
endif

.PHONY: get-cred-gpg

stage0: portage catalyst

.PHONY: stage0

.NOTPARALLEL: stage0

portage: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(CNTDIR) $@

.PHONY: sudo-test portage

catalyst: get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(CNTDIR) $@

.PHONY: catalyst

stage1: stage1-amd64-default

.PHONY: stage1

.NOTPARALLEL: stage1

stage1-amd64-default: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(STG1DIR) $@

.PHONY: stage1-amd64-default

stage2: stage2-amd64-default

.PHONY: stage2

.NOTPARALLEL: stage2

stage2-amd64-default: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(STG2DIR) $@

.PHONY: stage2-amd64-default

stage3: stage3-amd64-default

.PHONY: stage3

.NOTPARALLEL: stage3

stage3-amd64-default: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(STG3DIR) $@

.PHONY: stage3-amd64-default

stage4: stage4-amd64-default stage4-extra

.PHONY: stage4

.NOTPARALLEL: stage4

# Allow parallel of everything after base stage4
stage4-extra: installer stage4-amd64-default-zfs

.PHONY: stage4-extra

stage4-amd64-default: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(STG4DIR) $@

.PHONY: stage4-amd64-default

stage4-amd64-default-zfs: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(STG4DIR) $@

.PHONY: stage4-amd64-default-zfs

installer: sudo-test get-cred
	@sh tag_and_bag.sh $(QUSER) $(QPASS) $(CNTDIR) $@

.PHONY: installer

#!/bin/sh

[[ $# != 4 ]] && echo "Usage: <user> <pass> <containerfileDir> <image>"

set -e

QUSER=$1
QPASS=$2
CONTDIR=$3
CONTIMG=$4
DATE=$(date +%Y%m%d)
OPTS="--cap-add=CAP_SYS_ADMIN --cap-add=CAP_NET_ADMIN --cap-add=CAP_MKNOD --cap-add=CAP_SYS_PTRACE --pull-always"

podman login quay.io --username ${QUSER} --password ${QPASS}
skopeo login quay.io --username ${QUSER} --password ${QPASS}
podman build ${OPTS} -f ${CONTDIR}/${CONTIMG}.Containerfile --label quay.expires-after=8w -t reavesos:${CONTIMG}-${DATE} .
podman push "reavesos:${CONTIMG}-${DATE}" "quay.io/${QUSER}/reavesos:${CONTIMG}-${DATE}" --creds "${QUSER}:${QPASS}"
skopeo copy "docker://quay.io/${QUSER}/reavesos:${CONTIMG}-${DATE}" "docker://quay.io/${QUSER}/reavesos:${CONTIMG}"

#podman push ${SIGN_OPTS} "reavesos:${CONTIMG}-${DATE}" "quay.io/${QUSER}/reavesos:$@-${DATE}" --creds "${QUSER}:${QPASS}"

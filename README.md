# ReavesOS

Personal Linux Distro based on Gentoo

## Usage

Building and pushing is done with `make`.  If you want to rebuild everything,
simply run `sudo make`.  If you only want to build a specific stage (like
stage4), run `sudo make stage4`.  If you want a more specific target, run
`sudo make stage4-amd64-default-zfs`.  If you want to see all available
containers, run `make targets`.

## Targets

Installable targets are:

```
all
├── stage0
│   ├── portage
│   └── catalyst
├── stage1
│   └── stage1-amd64-default
├── stage2
│   └── stage2-amd64-default
├── stage3
│   └── stage3-amd64-default
└── stage4
    ├── stage4-amd64-default
    └── stage4-amd64-default-zfs
```

The default target is 'all'

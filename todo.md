TODO
====

Stages
------

* Make stage 3 default
* Make stage 4 defaults
    * generic
    * desktop generic
    * gnome
    * kde
* Make live cd stage 1 default
* Make live cd stage 2 default
* Figure out what's broken with stage 1 hardened selinux

Installer
---------

* Figure out how to run `install.sh` from a container
    * Possibly a generic stage4?
* Figure out how to list containers with label like 'Installable=yes'
* Get rid of Fyne
    * Need something that can be built statically
    * bubbletea?

Errata
------

* Figure out how to do 'filename' and 'stage' in one line in `build.sh`

Tests
-----

* Install to external HDD

subarch: amd64
target: stage1
version_stamp: openrc-latest
rel_type: default
profile: default/linux/amd64/17.1
snapshot_treeish: latest
source_subpath: default/stage3-amd64-openrc-latest-upstream.tar.xz
compression_mode: pixz_x
update_seed: yes
update_seed_command: --emptytree --ask n --exclude help2man @world
portage_confdir: /var/tmp/catalyst/portage/stage1
portage_prefix: reavessm
common_flags: "-O2 -pipe"

subarch: amd64
target: stage2
version_stamp: openrc-latest
rel_type: default
profile: default/linux/amd64/17.1
snapshot: latest
source_subpath: default/stage1-amd64-openrc-latest
compression_mode: pixz_x
portage_prefix: reavessm
portage_confdir: /var/tmp/catalyst/portage/stage2
portage_overlay: /var/db/repos/lto-overlay
portage_overlay: /var/db/repos/mv
portage_prefix: reavessm
common_flags: "-O2 -pipe"

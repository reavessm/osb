subarch: amd64
target: stage4
version_stamp: openrc-latest
rel_type: default
profile: default/linux/amd64/17.1
snapshot: latest
source_subpath: default/stage3-amd64-openrc-latest
compression_mode: pixz_x
portage_prefix: reavessm
portage_confdir: /var/tmp/catalyst/portage/stage4
#portage_overlay: /var/db/repos/lto-overlay
#portage_overlay: /var/db/repos/mv
portage_prefix: reavessm
common_flags: "-O2 -pipe"

boot/kernel: gentoo
boot/kernel/gentoo/sources: gentoo-sources:5.15.26
boot/kernel/gentoo/packages: "sys-boot/grub:2"

stage4/use: "filecaps vim -docs -doc -example bash-completion"
stage4/packages: "dhcpcd acpid sys-kernel/gentoo-sources:5.15.26 wpa_supplicant vim tmux dev-vcs/git htop"

stage4/gk_mainargs: --kernel-config=default --kerneldir=/usr/src/linux-5.15.26-gentoo

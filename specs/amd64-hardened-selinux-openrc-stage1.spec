subarch: amd64
target: stage1
version_stamp: hardened-selinux-openrc-latest
rel_type: hardened
profile: default/linux/amd64/17.1/hardened/selinux
snapshot: latest
source_subpath: hardened/stage3-amd64-hardened-selinux-openrc-latest-upstream.tar.xz
compression_mode: pixz_x
update_seed: yes
update_seed_command: --emptytree --ask n @world
portage_confdir: /var/tmp/catalyst/portage/stage1
#portage_overlay: /var/db/repos/lto-overlay
#portage_overlay: /var/db/repos/mv
portage_prefix: reavessm
#boot/kernel/gentoo/config /var/tmp/catalyst/kernel/config
#cflags: "-O3 -pipe -flto=4 -fgraphite-identity -floop-nest-optimize -fipa-pta -fno-semantic-interposition -fno-signed-zeros -fno-trapping-math -fassociative-math -freciprocal-math -fno-math-errno -ffinite-math-only -fno-rounding-math -fno-signaling-nans -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"
common_flags: "-O3 -pipe -flto=4 -fipa-pta -fno-semantic-interposition -fno-trapping-math -fno-math-errno -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"
#cflags: "-O3 -pipe -flto=4 -fipa-pta -fno-semantic-interposition -fno-trapping-math -fno-math-errno -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"
#cxxflags: "${CFLAGS}"
#cxxflags: "-O3 -pipe -flto=4 -fipa-pta -fno-semantic-interposition -fno-trapping-math -fno-math-errno -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"

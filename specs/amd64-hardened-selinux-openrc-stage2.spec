subarch: amd64
target: stage2
version_stamp: hardened-selinux-openrc-latest
rel_type: hardened
profile: default/linux/amd64/17.1/hardened/selinux
snapshot: latest
source_subpath: hardened/stage1-amd64-hardened-selinux-openrc-latest
compression_mode: pixz_x
portage_prefix: reavessm
portage_confdir: /var/tmp/catalyst/portage/stage2
portage_overlay: /var/db/repos/lto-overlay
portage_overlay: /var/db/repos/mv
portage_prefix: reavessm
common_flags: "-O3 -pipe -flto=4 -fgraphite-identity -floop-nest-optimize -fipa-pta -fno-semantic-interposition -fno-signed-zeros -fno-trapping-math -fassociative-math -freciprocal-math -fno-math-errno -ffinite-math-only -fno-rounding-math -fno-signaling-nans -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"

subarch: amd64
target: livecd-stage1
version_stamp: hardened-selinux-openrc-latest-livecd
rel_type: hardened
profile: default/linux/amd64/17.1/hardened/selinux
snapshot: latest
source_subpath: hardened/stage3-amd64-hardened-selinux-openrc-latest
compression_mode: pixz_x
#update_seed: yes
#update_seed_command: --update --deep --newuse @world
portage_prefix: reavessm
portage_confdir: /var/tmp/catalyst/portage/livecd-stage1
#portage_overlay: /var/db/repos/lto-overlay
#portage_overlay: /var/db/repos/mv
livecd/packages: 
	app-editors/vim 
	dev-vcs/git 
	app-misc/neofetch 
#	sys-config/ltoize 
	sys-kernel/gentoo-sources 
	livecd-tools 
	dhcpcd 
	acpid 
	apmd 
#	coldplug 
	fxload 
	irssi 
	wpa_supplicant
	sys-apps/util-linux
	baselayout
	pciutils
	links
	w3m
	#raidtools
	nfs-utils
	jfsutils
	usbutils
	xfsprogs
	xfsdump
	e2fsprogs
	reiserfsprogs
	hdparm
	nano
	less
	openssh
	dialog
	tmux
	app-misc/screen
	iputils
	mirrorselect
	#hwsetup
	#bootsplash
	#device-mapper
	lvm2
	gpart
	mdadm
	tcptraceroute
	netcat
	ethtool
	wireless-tools
	tcpdump
	nmap
#livecd/fstype: zisofs
common_flags: "-Os -pipe -flto=4 -fipa-pta -fno-semantic-interposition -fno-trapping-math -fno-math-errno -fcx-limited-range -fexcess-precision=fast -fdevirtualize-at-ltrans -fomit-frame-pointer -fuse-linker-plugin"

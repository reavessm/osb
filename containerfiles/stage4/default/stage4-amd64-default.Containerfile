FROM quay.io/reavessm/reavesos:stage3-amd64-default as builder

# Uncomment these to build against new specs without rebuilding previous stage
#COPY portage/ portage/
#COPY specs/ specs

RUN catalyst -pf specs/stage4/default/stage4-amd64-default.spec

WORKDIR /

RUN mkdir -p /mnt/gentoo \
  && tar xJvf /var/tmp/catalyst/builds/default/stage4*.xz -C /mnt/gentoo || true \
  && rm -rvf /var/tmp/catalyst

FROM scratch as temp

LABEL io.containers.capabilites=SYS_ADMIN,NET_ADMIN,MKNOD,SYS_PTRACE
LABEL quay.expires-after=8w
LABEL reavesOSInstallable=true

WORKDIR /

COPY --from=builder /mnt/gentoo/ /

RUN mkdir -p /etc/portage/repos.conf \
  && echo 'EMERGE_DEFAULT_OPTS="--quiet --ask --with-bdeps=y --autounmask=y --autounmask-continue"' >> /etc/portage/make.conf \
  && echo 'ACCEPT_KEYWORDS="~amd64"' >> /etc/portage/make.conf

COPY configs/gentoo.conf /etc/portage/repos.conf/gentoo.conf

RUN emerge --sync --ask n \
  && emerge --depclean --ask n --with-bdeps=n \
  && rm -rvf /var/db/repos/* \
  && mkdir -p /var/db/repos/gentoo \
  && rm -f /usr/src/linux \
  && ln -s /usr/src/linux-*-gentoo /usr/src/linux

FROM scratch

LABEL io.containers.capabilites=SYS_ADMIN,NET_ADMIN,MKNOD,SYS_PTRACE
LABEL reavesOSInstallable=true

WORKDIR /

COPY --from=temp / /

CMD ["/bin/bash"]

LABEL quay.expires-after=8w

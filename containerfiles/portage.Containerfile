FROM quay.io/reavessm/portage:latest as portage

FROM quay.io/reavessm/gentoo:latest

LABEL io.containers.capabilites=SYS_ADMIN,NET_ADMIN,MKNOD,SYS_PTRACE

# Test CAP_MKNOD
RUN mknod /dev/reavesos-test c 60 0 \
  && rm /dev/reavesos-test

COPY configs/gentoo.conf /etc/portage/repos.conf/gentoo.conf

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

WORKDIR /var/db/repos

ENV ACCEPT_KEYWORDS="~amd64"
ENV FEATURES="-pid-sandbox"

ENV EMERGE_DEFAULT_OPTS="--jobs 2 --load 2 --autounmask=y --autounmask-continue --with-bdeps=y --quiet --ask n -v"
ENV MAKEOPTS="-j2"

# Run with excludes
RUN mkdir -p /var/tmp/portage \
  && emerge \
  -uUDN \
  --exclude libxcrypt \
  --keep-going \
  @world \
  || true

# Run without excludes
RUN emerge \
  -uUDN \
  --keep-going \
  @world \
  || true

RUN emerge app-arch/pixz sys-libs/libomp

RUN emerge --ask n --depclean \
  && perl-cleaner --all

RUN emerge --ask n --quiet dev-vcs/git \
  && rm -rf gentoo \
  && git clone -v https://github.com/gentoo/gentoo.git

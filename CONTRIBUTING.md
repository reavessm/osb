# How to contribute?

Contributions will be rarely accepted until I can figure out what it is exactly I'm doing.  Thank you for understanding.

-smr

# Emoji list

:memo: -> Documentation changes

:rocket: -> Feature additions

:x: -> Deletions (comments or files)

:bug: :gun: -> Bug fixes

:construction: -> WIP

:beers: -> [Ballmer's Peak](https://xkcd.com/323/)

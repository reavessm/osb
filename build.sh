#!/bin/bash

# Written by: Stephen M. Reaves
# Created on: Mon, 13 Dec 2021

[[ ${EUID} != 0 ]] && echo "Please run as sudo as CAP_MKNOD is needed for stage builds" && exit 1

set -e

echo "Please enter quay.io credentials"
read -p "Username: " username
read -sp "Password: " password
echo

declare -a cfs

cfs[0]="containerfiles/portage.Containerfile"
#cfs[1]="containerfiles/catalyst.Containerfile"
#cfs[2]="containerfiles/stage1/default/stage1-amd64-default.Containerfile"
#cfs[3]="containerfiles/stage2/default/stage2-amd64-default.Containerfile"
#cfs[4]="containerfiles/stage3/default/stage3-amd64-default.Containerfile"
#cfs[5]="containerfiles/stage4/default/stage4-amd64-default.Containerfile"
#cfs[6]="containerfiles/stage4/default/stage4-amd64-default-zfs.Containerfile"

for cf in ${cfs[*]}
do
  filename="$(basename ${cf})"
  stage="${filename%.*}"

  echo "Building ${stage} ... <++>"

  podOpts="--cap-add=CAP_SYS_ADMIN"
  podOpts="${podOpts} --cap-add=CAP_NET_ADMIN"
  podOpts="${podOpts} --cap-add=CAP_MKNOD"
  podOpts="${podOpts} --cap-add=CAP_SYS_PTRACE"
  #podOpts="${podOpts} --isolation=oci"
  podOpts="${podOpts} --pull-always -f ${cf} -t reavesos:${stage}"

  podman build ${podOpts} .

  podman push "reavesos:${stage}" "quay.io/${username}/reavesos:${stage}" --creds "${username}:${password}"
  podman push "reavesos:${stage}" "quay.io/${username}/reavesos:${stage}-$(date +%Y%m%d)" --creds "${username}:${password}"

  echo "Done with ${stage} ... <++>"

done

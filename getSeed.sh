#!/bin/bash

# Written by: Stephen M. Reaves
# Created on: Thu, 27 Jan 2022

wget https://gentoo.osuosl.org/releases/amd64/autobuilds/"$(curl https://gentoo.osuosl.org/releases/amd64/autobuilds/latest-stage3-amd64-hardened-selinux-openrc.txt 2>/dev/null | tail -n 1 | awk '{print $1}')"

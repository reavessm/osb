#!/bin/bash

# File: createBuilds.sh
# Written by: Stephen M. Reaves
# Created on: Wed, 06 Apr 2022

oc project homelab-reavesos

oc delete is --all

# Local tags from external repos
oc tag docker.io/gentoo/stage3:latest gentoo-stage3:latest --scheduled
oc tag quay.io/reavessm/reavesos:portage reavesos:portage --scheduled
oc tag quay.io/reavessm/reavesos:catalyst reavesos:catalyst --scheduled
oc tag quay.io/reavessm/reavesos:stage1-amd64-default reavesos:stage1-amd64-default --scheduled
oc tag quay.io/reavessm/reavesos:stage2-amd64-default reavesos:stage2-amd64-default --scheduled
oc tag quay.io/reavessm/reavesos:stage3-amd64-default reavesos:stage3-amd64-default --scheduled
oc tag quay.io/reavessm/reavesos:stage4-amd64-default reavesos:stage4-amd64-default --scheduled

oc delete bc --all

# Portage
oc process -f template.yaml \
  -p PREV_CONTAINER_URL=gentoo-stage3 \
  -p PREV_STAGE=latest \
  | oc apply -f -

# Catalyst
oc process -f template.yaml \
  -p STAGE=catalyst \
  -p CONTAINERFILE_PATH=containerfiles/catalyst.Containerfile \
  -p PREV_STAGE=portage \
  | oc apply -f -

# Stage 1
oc process -f template.yaml \
  -p STAGE=stage1-amd64-default \
  -p CONTAINERFILE_PATH=containerfiles/stage1/default/stage1-amd64-default.Containerfile \
  -p PREV_STAGE=catalyst \
  | oc apply -f -

# Stage 2
oc process -f template.yaml \
  -p STAGE=stage2-amd64-default \
  -p CONTAINERFILE_PATH=containerfiles/stage2/default/stage2-amd64-default.Containerfile \
  -p PREV_STAGE=stage1-amd64-default \
  | oc apply -f -

# Stage 3
oc process -f template.yaml \
  -p STAGE=stage3-amd64-default \
  -p CONTAINERFILE_PATH=containerfiles/stage3/default/stage3-amd64-default.Containerfile \
  -p PREV_STAGE=stage2-amd64-default \
  | oc apply -f -

# Stage 4
oc process -f template.yaml \
  -p STAGE=stage4-amd64-default \
  -p CONTAINERFILE_PATH=containerfiles/stage4/default/stage4-amd64-default.Containerfile \
  -p PREV_STAGE=stage3-amd64-default \
  | oc apply -f -

oc start-build portage
